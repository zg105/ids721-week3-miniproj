
# Week3 mini Project zg105

In this project, I created an S3 Bucket using CDK with AWS CodeWhisperer. 

The `cdk.json` file tells the CDK Toolkit how to execute your app.

## Getting started

To start with, I need add CodeWhisper to vscode extension and log into my AWS account.

Then, run `npm install -g aws-cdk` to install aws cdk.

Then run `cdk init app --language=typescript` to init an app using python typescript.

Modified the app in `stack.ts` file with the help of CodeWhisper, which provided code prompt for us.
You can add the encryption and version configuration.

Then run `cdk deploy` to deploy your app

![](1.png)

After deployment, we can check the S3 bucket in AWS console:

![](2.png)

